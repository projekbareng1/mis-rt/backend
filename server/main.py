import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
import sys

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)

from server.database.session import engine
from server.routers import auth_router, application_router, user_router, payment_router, funding_router
from server.dependencies import authentication
from server.database.session import Base

app = FastAPI()
app.include_router(authentication.router)
app.include_router(auth_router.router)
app.include_router(user_router.router)
app.include_router(application_router.router)
app.include_router(payment_router.router)
app.include_router(funding_router.router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    expose_headers=["*"]
)

# CORS(app, include_health_check=False)

Base.metadata.create_all(engine)

if __name__ == '__main__':
    uvicorn.run("main:app", port=8000, host='127.0.0.1', reload=True)
