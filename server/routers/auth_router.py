from fastapi import APIRouter, HTTPException, Depends, status
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session
import os
import sys

from server.models.UserModel import User
from server.models.AuthSchema import Login
from server.models.AuthSchema import CheckLogin
from server.dependencies.hash import Hash
from server.database.session import get_db
from server.database.repositories import UserRepository
from server.dependencies import oauth2
from server.models.UserSchema import UserOut
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)

router = APIRouter(
    prefix='',
    tags=['auth']
)


@router.post('/login/')
async def login(request: Login, db: Session = Depends(get_db)):
    user = db.query(User).filter_by(username=request.username).first()
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid credentials")
    if not Hash.verify(user.password, request.password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Incorrect password")

    access_token = oauth2.create_access_token(data={'sub': user.username})

    return JSONResponse({
        'access_token': access_token,
        'token_type': 'bearer',
        'user_id': user.id,
        'username': user.username
    })


@router.post('/check-login/', response_model=UserOut)
async def check_login(request: CheckLogin, db: Session = Depends(get_db)):
    user = oauth2.get_current_user(request.auth_key, db)

    return user
