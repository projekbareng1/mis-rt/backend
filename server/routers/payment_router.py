from pathlib import Path
from fastapi import APIRouter, Depends, HTTPException, Query
from fastapi.responses import FileResponse
from sqlalchemy.orm import Session
from typing import List
import os
import sys

from server.database.session import get_db
from server.dependencies.oauth2 import get_current_user
from server.models.UserSchema import UserOut
from server.models.PaymentSchema import PaymentIn, PaymentInUpdate, PaymentOut
from server.database.repositories import PaymentRepository, FundingRepository

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)

router = APIRouter(
    prefix='/payment',
    tags=['payment']
)


@router.get('/{funding_id}/funding', response_model=List[PaymentOut])
async def index(funding_id: int, db: Session = Depends(get_db), search: str = Query('', alias="search"), page: int = Query(1, alias="page", ge=1), limit: int = Query(10, le=100), current_user: UserOut = Depends(get_current_user)):
    start_index = (page * limit) - limit
    end_index = start_index + limit
    payments = PaymentRepository.get_all_payment(
        db, funding_id, search, current_user)
    items = payments[start_index:end_index]
    return items


@router.post('/{funding_id}/funding', response_model=PaymentOut)
async def store(funding_id: int, request: PaymentIn = Depends(), db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    funding = FundingRepository.get_funding_by_id(db, funding_id, current_user)
    funding_name = funding.title
    doc = PaymentRepository.create_payment(
        db, funding_id, funding_name, request, current_user)  # type: ignore
    return doc


@router.put('/{id}/')
async def update(id: int, request: PaymentInUpdate = Depends(), db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    doc = PaymentRepository.update_payment_by_id(
        db, id, request, current_user)  # type: ignore
    return doc


@router.put('/{id}/approve')
async def approve(id: int, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    print(current_user)
    doc = PaymentRepository.approve_payment_by_id(
        db, id, current_user)  # type: ignore
    return doc


@router.put('/{id}/reject')
async def reject(id: int, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    doc = PaymentRepository.reject_payment_by_id(
        db, id, current_user)  # type: ignore
    return doc


@router.get('/{id}/download')
async def download(id: int, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    doc = PaymentRepository.get_payment_by_id(db, id, current_user)
    if (doc):
        filename = f'bukti'
        file_path = f'./{doc.file_path}'
        if (Path(file_path).exists()):
            return FileResponse(file_path, filename=f'{filename}.pdf')
        raise HTTPException(
            status_code=404, detail=f'File not found for standarize with id {id}'
        )


@router.get('/{id}/', response_model=PaymentOut)
async def show(id: int, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    doc = PaymentRepository.get_payment_by_id(db, id, current_user)
    return doc


@router.delete('/{id}/')
async def destroy(id: int, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    response = PaymentRepository.delete_payment_by_id(
        db, id, current_user)
    return response
