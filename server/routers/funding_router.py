from pathlib import Path
from fastapi import APIRouter, Depends, HTTPException, Query
from fastapi.responses import FileResponse
from sqlalchemy.orm import Session
from typing import List
import os
import sys

from server.database.session import get_db
from server.dependencies.oauth2 import get_current_user
from server.models.PaymentModel import StatusEnum
from server.models.UserSchema import UserOut
from server.models.FundingSchema import FundingIn, FundingInUpdate, FundingOut, FundingReport
from server.database.repositories import FundingRepository

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)

router = APIRouter(
    prefix='/funding',
    tags=['funding']
)


@router.get('/', response_model=List[FundingOut])
async def index(db: Session = Depends(get_db), search: str = Query('', alias="search"), page: int = Query(1, alias="page", ge=1), limit: int = Query(10, le=100), current_user: UserOut = Depends(get_current_user)):
    start_index = (page * limit) - limit
    end_index = start_index + limit
    fundings = FundingRepository.get_all_funding(
        db, search, current_user)
    items = fundings[start_index:end_index]
    for item in items:
        filtered = [
            payment for payment in item.payments if payment.status != StatusEnum.REJECTED]
        item.payments = filtered
    return items


@router.post('/')
async def store(request: FundingIn, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    doc = FundingRepository.create_funding(
        db, request, current_user)
    return doc


@router.post('/{id}/report')
async def store_report(id: int, request: FundingReport, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    funding = FundingRepository.store_report(db, request, id)
    return funding


@router.put('/{id}/')
async def update(id: int, request: FundingInUpdate, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    doc = FundingRepository.update_funding_by_id(
        db, id, request, current_user)  # type: ignore
    return doc


@router.get('/{id}/', response_model=FundingOut)
async def show(id: int, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    doc = FundingRepository.get_funding_by_id(db, id, current_user)
    return doc


@router.delete('/{id}/')
async def destroy(id: int, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    response = FundingRepository.delete_funding_by_id(
        db, id, current_user)
    return response
