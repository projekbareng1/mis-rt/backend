from datetime import date
from pathlib import Path
from fastapi import APIRouter, Depends, HTTPException, Query
from fastapi.responses import FileResponse
from sqlalchemy.orm import Session
from typing import List
import os
import sys

from server.database.session import get_db
from server.dependencies.oauth2 import get_current_user
from server.models.UserSchema import UserIn, UserOut
from server.models.ApplicationSchema import ApplicationIn, ApplicationInUpdate, ApplicationOut
from server.database.repositories import ApplicationRepository
from server.services import create_pdf

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)

router = APIRouter(
    prefix='/application',
    tags=['application']
)


@router.get('/', response_model=List[ApplicationOut])
async def index(db: Session = Depends(get_db), search: str = Query('', alias="search"), page: int = Query(1, alias="page", ge=1), limit: int = Query(10, le=100), current_user: UserOut = Depends(get_current_user)):
    start_index = (page * limit) - limit
    end_index = start_index + limit
    applications = ApplicationRepository.get_all_application(
        db, search, current_user)
    items = applications[start_index:end_index]
    return items


@router.post('/')
async def store(request: ApplicationIn, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    doc = ApplicationRepository.create_application(
        db, request, current_user)
    return doc


@router.put('/{id}/')
async def update(id: int, request: ApplicationInUpdate, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    doc = ApplicationRepository.update_application_by_id(
        db, id, request, current_user)  # type: ignore
    return doc


@router.put('/{id}/approve')
async def approve(id: int, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    print(current_user)
    doc = ApplicationRepository.approve_application_by_id(
        db, id, current_user)  # type: ignore
    return doc


@router.put('/{id}/reject')
async def reject(id: int, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    doc = ApplicationRepository.reject_application_by_id(
        db, id, current_user)  # type: ignore
    return doc


@router.get('/{id}/report')
async def download(id: int, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    application = ApplicationRepository.get_application_by_id(
        db, id, current_user)

    data = {
        "name": application.owner.name,
        "gender": application.owner.gender.value,
        "birth_place": application.owner.birth_place,
        "birth_date": application.owner.birth_date.split()[0],
        "nik": application.owner.nik,
        "nationality": application.owner.nationality,
        "religion": application.owner.religion,
        "job": application.owner.job,
        "address": application.owner.address,
        "description": application.description,
        "current_date": str(date.today())
    }
    pdf_path = create_pdf('application_report/index.html', data)
    return FileResponse(
        path=pdf_path,
        headers={
            'Content-Disposition': f'inline;filename="{pdf_path}.pdf"'
        },
        media_type='application/pdf')


@router.get('/{id}/', response_model=ApplicationOut)
async def show(id: int, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    doc = ApplicationRepository.get_application_by_id(db, id, current_user)
    return doc


@router.delete('/{id}/')
async def destroy(id: int, db: Session = Depends(get_db), current_user: UserOut = Depends(get_current_user)):
    response = ApplicationRepository.delete_application_by_id(
        db, id, current_user)
    return response
