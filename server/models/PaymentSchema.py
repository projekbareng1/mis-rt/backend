from datetime import datetime
from typing import Optional
from pydantic import BaseModel, EmailStr, validator
from fastapi import File, Form, HTTPException, UploadFile

from server.models.PaymentModel import StatusEnum


class User(BaseModel):
    id: int
    name: str
    email: EmailStr


class PaymentIn(BaseModel):
    file: UploadFile

    @validator("file")
    def validate_files(cls, file):
        if not file.filename.lower().endswith('.pdf'):
            raise HTTPException(
                status_code=422, detail="Only PDF files are allowed")
        return file


class PaymentInUpdate(BaseModel):
    file: Optional[UploadFile] = File(None)

    @validator("file")
    def validate_files(cls, file):
        if file and (not file.filename.lower().endswith('.pdf')):
            raise HTTPException(
                status_code=422, detail="Only PDF files are allowed")
        return file


class PaymentOut(BaseModel):
    id: int
    status: StatusEnum
    file_path: Optional[str]
    created_at: datetime
    owner: User

    class Config():
        from_attributes = True
        arbitary_types = True
        json_encoders = {
            datetime: lambda v: v.isoformat()
        }
