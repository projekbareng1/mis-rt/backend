from fastapi import HTTPException, status


class ApplicationHashTable:
    def __init__(self, table_size=100):
        self.table_size = table_size
        self.hash_table = [[] for _ in range(table_size)]

    def reinit(self):
        self.hash_table = [[] for _ in range(self.table_size)]

    def _hash_function(self, application_id):
        return hash(application_id) % self.table_size

    def insert_application(self, application_id, application):
        index = self._hash_function(application_id)
        self.hash_table[index].append((application_id, application))

    def get_application_by_id(self, application_id):
        index = self._hash_function(application_id)
        bucket = self.hash_table[index]
        for doc_id, application in bucket:
            if doc_id == application_id:
                return application
        return None

    def delete_application(self, application_id):
        index = self._hash_function(application_id)
        bucket = self.hash_table[index]

        for i, (doc_id, application) in enumerate(bucket):
            if doc_id == application_id:
                del bucket[i]
                return

        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Application with id {application_id} not found')
