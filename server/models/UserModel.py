import enum
from sqlalchemy import Enum, Float, Integer, String, Column, DateTime, ForeignKey, null
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func
import os
import sys

from server.database.session import Base

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)


class CategoryEnum(enum.Enum):
    ATAS = "atas"
    MENENGAH = "menengah"
    BAWAH = "bawah"


class GenderEnum(enum.Enum):
    MALE = 'male'
    FEMALE = 'female'


class Role(Base):
    __table_args__ = {'extend_existing': True}
    __tablename__ = 'roles'
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255), nullable=False)

    user = relationship("User", back_populates='role', viewonly=True)


class User(Base):
    __table_args__ = {'extend_existing': True}
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255), nullable=False)
    gender = Column(Enum(GenderEnum), nullable=False)
    birth_place = Column(String(255), nullable=False)
    birth_date = Column(String(255), nullable=True)
    nik = Column(String(255), unique=True, nullable=False)
    nationality = Column(String(255), nullable=False)
    religion = Column(String(255), nullable=False)
    job = Column(String(255), nullable=False)
    address = Column(String(255), nullable=False)
    username = Column(String(255), unique=True, nullable=False)
    email = Column(String(255), unique=True, nullable=False)
    password = Column(String(255), nullable=False)
    salary = Column(Float, nullable=False)
    category = Column(Enum(CategoryEnum))
    role_id = Column(Integer, ForeignKey("roles.id"), nullable=False)
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    applications = relationship(
        "Application", back_populates='owner', viewonly=True)
    payments = relationship(
        "Payment", back_populates='owner', viewonly=True)
    fundings = relationship("Funding", back_populates='owner', viewonly=True)
    role = relationship("Role", back_populates='user', viewonly=True)
