from fastapi import HTTPException, status


class FundingHashTable:
    def __init__(self, table_size=100):
        self.table_size = table_size
        self.hash_table = [[] for _ in range(table_size)]

    def reinit(self):
        self.hash_table = [[] for _ in range(self.table_size)]

    def _hash_function(self, funding_id):
        return hash(funding_id) % self.table_size

    def insert_funding(self, funding_id, funding):
        index = self._hash_function(funding_id)
        self.hash_table[index].append((funding_id, funding))

    def get_funding_by_id(self, funding_id):
        index = self._hash_function(funding_id)
        bucket = self.hash_table[index]
        for doc_id, funding in bucket:
            if doc_id == funding_id:
                return funding
        return None

    def delete_funding(self, funding_id):
        index = self._hash_function(funding_id)
        bucket = self.hash_table[index]

        for i, (doc_id, funding) in enumerate(bucket):
            if doc_id == funding_id:
                del bucket[i]
                return

        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Funding with id {funding_id} not found')
