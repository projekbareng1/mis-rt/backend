import enum
from datetime import datetime
from typing import List, Optional
from pydantic import BaseModel, EmailStr, validator
from fastapi import File, Form, HTTPException, UploadFile

from server.models.PaymentModel import StatusEnum


class User(BaseModel):
    id: int
    name: str
    email: EmailStr


class Payment(BaseModel):
    status: StatusEnum
    owner: User


class FundingIn(BaseModel):
    title: str
    nominal: float

    @validator("title", pre=True)
    def validate_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Input not allowed to be empty")
        return value


class FundingReport(BaseModel):
    report: str

    @validator("report", pre=True)
    def validate_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Input not allowed to be empty"
            )
        return value


class FundingInUpdate(BaseModel):
    title: str
    nominal: float

    @validator("title", pre=True)
    def validate_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Input not allowed to be empty")
        return value


class FundingOut(BaseModel):
    id: int
    title: str
    nominal: float
    created_at: datetime
    owner: User
    payments: List[Payment]
    report: Optional[str]

    class Config():
        from_attributes = True
        arbitary_types = True
        json_encoders = {
            datetime: lambda v: v.isoformat()
        }
