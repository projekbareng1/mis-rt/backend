from enum import Enum
from fastapi import HTTPException
from pydantic import BaseModel, EmailStr, validator
from typing import List, Optional
import os
import sys


from server.database.session import SessionLocal
from server.models.PaymentModel import StatusEnum
from server.models.UserModel import CategoryEnum, GenderEnum, User

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)


class Application(BaseModel):
    description: Optional[str]


class Funding(BaseModel):
    title: str
    nominal: float


class Payment(BaseModel):
    status: StatusEnum
    funding: Funding
    owner_id: int


class Role(BaseModel):
    name: str


class UserIn(BaseModel):
    name: str
    gender: GenderEnum
    birth_place: str
    birth_date: str
    nik: str
    nationality: str
    religion: str
    job: str
    address: str
    username: str
    email: EmailStr
    password: str
    salary: float
    role: str

    @validator("name", pre=True)
    def validate_name_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Name input not allowed to be empty")
        return value

    @validator("username", pre=True)
    def validate_username_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("gender", pre=True)
    def validate_gender_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("birth_place", pre=True)
    def validate_birth_place_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("birth_date", pre=True)
    def validate_birth_date_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("nik", pre=True)
    def validate_nik_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("nationality", pre=True)
    def validate_nationality_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("religion", pre=True)
    def validate_religion_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("job", pre=True)
    def validate_job_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("address", pre=True)
    def validate_address_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("email", pre=True)
    def validate_email_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Email input not allowed to be empty")
        return value

    @validator("password", pre=True)
    def validate_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Password input not allowed to be empty")
        return value

    @validator("username")
    def validate_username_unique(cls, username):
        db = SessionLocal()
        user = db.query(User).filter(User.username == username).first()
        db.close()
        if user:
            raise ValueError("Username already exists")
        return username

    @validator("nik")
    def validate_nik_unique(cls, nik):
        db = SessionLocal()
        user = db.query(User).filter(User.nik == nik).first()
        db.close()
        if user:
            raise ValueError("Username already exists")
        return nik

    @validator("email")
    def validate_email_unique(cls, email):
        db = SessionLocal()
        user = db.query(User).filter(User.email == email).first()
        db.close()
        if user:
            raise ValueError("Email already exists")
        return email

    @validator("role")
    def validate_role_type(cls, role):
        if role not in ['warga', 'rt']:
            raise ValueError(
                "Invalid role value. Allowed values are: 'warga', 'rt'")
        return role


class UserInUpdate(BaseModel):
    name: str
    gender: GenderEnum
    birth_place: str
    birth_date: str
    nik: str
    nationality: str
    religion: str
    job: str
    address: str
    username: str
    email: EmailStr
    password: Optional[str]
    salary: float
    category: Optional[CategoryEnum]
    role: str

    @validator("name", pre=True)
    def validate_name_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Name input not allowed to be empty")
        return value

    @validator("username", pre=True)
    def validate_username_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("gender", pre=True)
    def validate_gender_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("birth_place", pre=True)
    def validate_birth_place_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("birth_date", pre=True)
    def validate_birth_date_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("nik", pre=True)
    def validate_nik_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("nationality", pre=True)
    def validate_nationality_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("religion", pre=True)
    def validate_religion_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("job", pre=True)
    def validate_job_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("address", pre=True)
    def validate_address_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Username input not allowed to be empty")
        return value

    @validator("email", pre=True)
    def validate_email_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Email input not allowed to be empty")
        return value

    @validator("role")
    def validate_role_type(cls, role):
        if role not in ['warga', 'rt']:
            raise ValueError(
                "Invalid fruit value. Allowed values are: 'warga', 'rt'")
        return role


class UserOut(BaseModel):
    id: int
    name: str
    gender: GenderEnum
    birth_place: str
    birth_date: str
    nik: str
    nationality: str
    religion: str
    job: str
    address: str
    username: str
    email: EmailStr
    salary: float
    category: Optional[CategoryEnum]
    applications: List[Application]
    payments: List[Payment]
    fundings: List[Funding]
    role: Role

    class Config():
        from_attributes = True
        arbitary_types = True
