from datetime import datetime
from typing import Optional
from pydantic import BaseModel, EmailStr, validator
from fastapi import HTTPException

from server.models.ApplicationModel import StatusEnum


class User(BaseModel):
    id: int
    name: str
    email: EmailStr


class ApplicationIn(BaseModel):
    description: str

    @validator("description", pre=True)
    def validate_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Input not allowed to be empty")
        return value


class ApplicationInUpdate(BaseModel):
    description: str
    status: str

    @validator("description", pre=True)
    def validate_not_null(cls, value):
        if value is None or value.strip() == "":
            raise HTTPException(
                status_code=422, detail="Input not allowed to be empty")
        return value

    @validator("status")
    def validate_role_type(cls, role):
        if role not in ['pending', 'approved', 'rejected']:
            raise ValueError(
                "Invalid fruit value. Allowed values are: 'pending', 'approved', 'rejected'")
        return role


class ApplicationOut(BaseModel):
    id: int
    description: str
    status: StatusEnum
    created_at: datetime
    owner: User

    class Config():
        from_attributes = True
        arbitary_types = True
        json_encoders = {
            datetime: lambda v: v.isoformat()
        }
