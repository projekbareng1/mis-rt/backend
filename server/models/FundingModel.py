from sqlalchemy import Float, Integer, String, Column, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func
import enum
import os
import sys

from server.database.session import Base

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)


class StatusEnum(enum.Enum):
    PENDING = "pending"
    APPROVED = "approved"
    REJECTED = "rejected"


class Funding(Base):
    __table_args__ = {'extend_existing': True}
    __tablename__ = "fundings"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(255), nullable=False)
    nominal = Column(Float)
    report = Column(String(255))
    owner_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    owner = relationship("User", back_populates='fundings', viewonly=True)
    payments = relationship("Payment", back_populates='funding', viewonly=True)
    # funding_reports = relationship(
    # "FundingReport", back_populates='funding', viewonly=True)
