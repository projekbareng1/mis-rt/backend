from sqlalchemy import Column, DateTime, ForeignKey
from sqlalchemy.types import Enum, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func
import enum
import os
import sys

from server.database.session import Base

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)


class StatusEnum(enum.Enum):
    PENDING = "pending"
    APPROVED = "approved"
    REJECTED = "rejected"


class Application(Base):
    __table_args__ = {'extend_existing': True}
    __tablename__ = "applications"
    id = Column(Integer, primary_key=True, index=True)
    description = Column(String(255))
    status = Column(Enum(StatusEnum), nullable=False)
    owner_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    owner = relationship("User", back_populates='applications', viewonly=True)
