from fastapi import HTTPException, status


class PaymentHashTable:
    def __init__(self, table_size=100):
        self.table_size = table_size
        self.hash_table = [[] for _ in range(table_size)]

    def reinit(self):
        self.hash_table = [[] for _ in range(self.table_size)]

    def _hash_function(self, payment_id):
        return hash(payment_id) % self.table_size

    def insert_payment(self, payment_id, payment):
        index = self._hash_function(payment_id)
        self.hash_table[index].append((payment_id, payment))

    def get_payment_by_id(self, payment_id):
        index = self._hash_function(payment_id)
        bucket = self.hash_table[index]
        for doc_id, payment in bucket:
            if doc_id == payment_id:
                return payment
        return None

    def delete_payment(self, payment_id):
        index = self._hash_function(payment_id)
        bucket = self.hash_table[index]

        for i, (doc_id, payment) in enumerate(bucket):
            if doc_id == payment_id:
                del bucket[i]
                return

        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Payment with id {payment_id} not found')
