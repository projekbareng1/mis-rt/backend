import enum
from sqlalchemy import Enum, Float, Integer, String, Column, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func
import os
import sys

from server.database.session import Base

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)


class StatusEnum(enum.Enum):
    PENDING = "pending"
    APPROVED = "approved"
    REJECTED = "rejected"


class Payment(Base):
    __table_args__ = {'extend_existing': True}
    __tablename__ = "payments"
    id = Column(Integer, primary_key=True, index=True)
    status = Column(Enum(StatusEnum), nullable=False)
    owner_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    funding_id = Column(Integer, ForeignKey("fundings.id"), nullable=False)
    file_path = Column(String(255), unique=True, nullable=False)
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    owner = relationship("User", back_populates='payments', viewonly=True)
    funding = relationship("Funding", back_populates='payments', viewonly=True)
