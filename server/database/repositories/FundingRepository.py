from operator import or_
from fastapi import HTTPException, status
from sqlalchemy.orm.session import Session
from sqlalchemy import or_
import os
import sys

from server.models.FundingModel import Funding, StatusEnum
from server.models.FundingSchema import FundingIn, FundingInUpdate, FundingOut, FundingReport
from server.models.FundingHashTable import FundingHashTable
from server.models.UserModel import User
from server.models.UserSchema import UserOut

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)


funding_hash_table = FundingHashTable()


def get_all_funding(db: Session, search: str, current_user: UserOut):
    base_query = db.query(Funding)
    if search:
        base_query = base_query.filter(
            or_(
                Funding.title.ilike(f"%{search}%"),
                Funding.description.ilike(f"%{search}%"),
            )
        )

    fundings = base_query.all()

    funding_hash_table.reinit()
    for funding in fundings:
        funding_hash_table.insert_funding(funding.id, funding)
    return fundings


def create_funding(db: Session, request: FundingIn, current_user: UserOut):
    owner_id = current_user.id

    new_doc = Funding(
        title=request.title,
        nominal=request.nominal,
        owner_id=owner_id,
    )
    db.add(new_doc)
    db.commit()
    db.refresh(new_doc)
    return new_doc


def store_report(db: Session, request: FundingReport, funding_id: int):
    funding = db.query(Funding).filter_by(id=funding_id)
    if not funding:
        raise HTTPException(
            status_code=404, detail="Data not found"
        )

    funding.update({
        Funding.report: request.report
    })
    db.commit()
    funding = db.query(Funding).filter_by(id=funding_id).first()
    if funding:
        hashed_doc = funding_hash_table.get_funding_by_id(
            funding.id)
        if hashed_doc:
            funding_hash_table.delete_funding(funding.id)

    return funding


def get_funding_by_id(db: Session, id: int, current_user: UserOut):
    funding = funding_hash_table.get_funding_by_id(id)
    if funding:
        return funding
    funding = db.query(Funding).filter_by(id=id).first()
    if not funding:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Funding with id {id} not found')
    funding_hash_table.insert_funding(funding.id, funding)
    return funding


def update_funding_by_id(db: Session, funding_id: int, request: FundingInUpdate, current_user: UserOut):
    funding = db.query(Funding).filter_by(id=funding_id)
    if not funding:
        raise HTTPException(
            status_code=404, detail="Data not found"
        )

    owner_id = current_user.id

    funding.update({
        Funding.title: request.title,
        Funding.nominal: request.nominal,
        Funding.owner_id: owner_id,
    })

    db.commit()
    funding = db.query(Funding).filter_by(id=funding_id).first()
    if funding:
        hashed_doc = funding_hash_table.get_funding_by_id(
            funding.id)
        if hashed_doc:
            funding_hash_table.delete_funding(funding.id)

    return funding


def approve_funding_by_id(db: Session, funding_id: int, current_user: UserOut):
    if (current_user.role.name != 'rt'):
        raise HTTPException(
            status_code=401, detail="Approve only can be done by admin"
        )

    funding = db.query(Funding).filter_by(id=funding_id)
    if not funding:
        raise HTTPException(
            status_code=404, detail="Data not found"
        )

    funding.update({
        Funding.status: StatusEnum["APPROVED"]
    })

    db.commit()

    funding = db.query(Funding).filter_by(id=funding_id).first()
    if funding:
        hashed_doc = funding_hash_table.get_funding_by_id(
            funding.id)
        if hashed_doc:
            funding_hash_table.delete_funding(funding.id)

    return funding


def reject_funding_by_id(db: Session, funding_id: int, current_user: UserOut):
    if (current_user.role.name != 'rt'):
        raise HTTPException(
            status_code=401, detail="Reject only can be done by admin"
        )

    funding = db.query(Funding).filter_by(id=funding_id)
    if not funding:
        raise HTTPException(
            status_code=404, detail="Data not found"
        )

    funding.update({
        Funding.status: StatusEnum["REJECTED"]
    })

    db.commit()

    funding = db.query(Funding).filter_by(id=funding_id).first()
    if funding:
        hashed_doc = funding_hash_table.get_funding_by_id(
            funding.id)
        if hashed_doc:
            funding_hash_table.delete_funding(funding.id)

    return funding


def delete_funding_by_id(db: Session, id: int, current_user: UserOut):
    funding_hash_table.delete_funding(id)
    funding = get_funding_by_id(db, id, current_user)
    if funding:
        db.delete(funding)
        db.commit()
        return funding
    else:
        raise HTTPException(
            status_code=200, detail=f"Funding with ID {id} not found")
