from operator import or_
from fastapi import HTTPException, status
from sqlalchemy.orm.session import Session
from sqlalchemy import or_
import os
import sys

from server.models.ApplicationModel import Application, StatusEnum
from server.models.ApplicationSchema import ApplicationIn, ApplicationInUpdate, ApplicationOut
from server.models.ApplicationHashTable import ApplicationHashTable
from server.models.UserModel import User
from server.models.UserSchema import UserOut

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)


application_hash_table = ApplicationHashTable()


def get_all_application(db: Session, search: str, current_user: UserOut):
    base_query = db.query(Application)
    if search:
        base_query = base_query.filter(
            or_(
                Application.description.ilike(f"%{search}%"),
            )
        )
    if (current_user.role.name == 'rt'):
        applications = base_query.all()
    else:
        applications = base_query.join(
            User, Application.owner_id == User.id).filter(User.role == current_user.role).all()

    application_hash_table.reinit()
    for application in applications:
        application_hash_table.insert_application(application.id, application)
    return applications


def create_application(db: Session, request: ApplicationIn, current_user: UserOut):
    owner_id = current_user.id

    new_doc = Application(
        description=request.description,
        status=StatusEnum["PENDING"],  # type: ignore
        owner_id=owner_id,
    )
    db.add(new_doc)
    db.commit()
    db.refresh(new_doc)
    return new_doc


def get_application_by_id(db: Session, id: int, current_user: UserOut):
    application = application_hash_table.get_application_by_id(id)
    if application:
        return application
    application = db.query(Application).filter_by(id=id).first()
    if not application:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Application with id {id} not found')
    application_hash_table.insert_application(application.id, application)
    return application


def update_application_by_id(db: Session, application_id: int, request: ApplicationInUpdate, current_user: UserOut):
    application = db.query(Application).filter_by(id=application_id)
    if not application:
        raise HTTPException(
            status_code=404, detail="Data not found"
        )

    owner_id = current_user.id

    application.update({
        Application.description: request.description,
        Application.status: StatusEnum[request.status.upper()],  # type: ignore
        Application.owner_id: owner_id,
    })

    db.commit()
    application = db.query(Application).filter_by(id=application_id).first()
    if application:
        hashed_doc = application_hash_table.get_application_by_id(
            application.id)
        if hashed_doc:
            application_hash_table.delete_application(application.id)

    return application


def approve_application_by_id(db: Session, application_id: int, current_user: UserOut):
    if (current_user.role.name != 'rt'):
        raise HTTPException(
            status_code=401, detail="Approve only can be done by admin"
        )

    application = db.query(Application).filter_by(id=application_id)
    if not application:
        raise HTTPException(
            status_code=404, detail="Data not found"
        )

    application.update({
        Application.status: StatusEnum["APPROVED"]
    })

    db.commit()

    application = db.query(Application).filter_by(id=application_id).first()
    if application:
        hashed_doc = application_hash_table.get_application_by_id(
            application.id)
        if hashed_doc:
            application_hash_table.delete_application(application.id)

    return application


def reject_application_by_id(db: Session, application_id: int, current_user: UserOut):
    if (current_user.role.name != 'rt'):
        raise HTTPException(
            status_code=401, detail="Reject only can be done by admin"
        )

    application = db.query(Application).filter_by(id=application_id)
    if not application:
        raise HTTPException(
            status_code=404, detail="Data not found"
        )

    application.update({
        Application.status: StatusEnum["REJECTED"]
    })

    db.commit()

    application = db.query(Application).filter_by(id=application_id).first()
    if application:
        hashed_doc = application_hash_table.get_application_by_id(
            application.id)
        if hashed_doc:
            application_hash_table.delete_application(application.id)

    return application


def delete_application_by_id(db: Session, id: int, current_user: UserOut):
    application_hash_table.delete_application(id)
    application = get_application_by_id(db, id, current_user)
    if application:
        db.delete(application)
        db.commit()
        return application
    else:
        raise HTTPException(
            status_code=200, detail=f"Application with ID {id} not found")
