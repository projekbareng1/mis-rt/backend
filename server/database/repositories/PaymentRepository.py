from operator import or_
from fastapi import HTTPException, status
from sqlalchemy.orm.session import Session
from sqlalchemy import or_
import os
import sys

from server.models.PaymentModel import Payment, StatusEnum
from server.models.PaymentSchema import PaymentIn, PaymentInUpdate, PaymentOut
from server.models.PaymentHashTable import PaymentHashTable
from server.models.UserModel import User
from server.models.UserSchema import UserOut

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)


payment_hash_table = PaymentHashTable()


def get_all_payment(db: Session, funding_id: int, search: str, current_user: UserOut):
    base_query = db.query(Payment)
    if search:
        base_query = base_query.filter(
            or_(
                Payment.title.ilike(f"%{search}%"),
                Payment.description.ilike(f"%{search}%"),
            )
        )
    payments = base_query.filter_by(funding_id=funding_id).all()
    payment_hash_table.reinit()
    for payment in payments:
        payment_hash_table.insert_payment(payment.id, payment)
    return payments


def create_payment(db: Session, funding_id: int, funding_name: str, request: PaymentIn, current_user: UserOut):
    owner_id = current_user.id
    payments = get_all_payment(db, funding_id, '', current_user)
    generated_filename = generate_unique_filename(
        f'{current_user.name}_{funding_name}', payments, owner_id)

    file_path = f'{generated_filename}.pdf'
    os.makedirs(os.path.dirname(file_path), exist_ok=True)
    with open(file_path, "wb") as file_object:
        file_object.write(request.file.file.read())

    new_payment = Payment(
        status='PENDING',
        funding_id=funding_id,
        owner_id=owner_id,
        file_path=file_path
    )
    db.add(new_payment)
    db.commit()
    db.refresh(new_payment)
    return new_payment


def get_payment_by_id(db: Session, id: int, current_user: UserOut):
    payment = payment_hash_table.get_payment_by_id(id)
    if payment:
        return payment
    payment = db.query(Payment).filter_by(id=id).first()
    if not payment:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Payment with id {id} not found')
    payment_hash_table.insert_payment(payment.id, payment)
    return payment


def approve_payment_by_id(db: Session, payment_id: int, current_user: UserOut):
    if (current_user.role.name != 'rt'):
        raise HTTPException(
            status_code=401, detail="Approve only can be done by admin"
        )

    payment = db.query(Payment).filter_by(id=payment_id)
    if not payment:
        raise HTTPException(
            status_code=404, detail="Data not found"
        )

    payment.update({
        Payment.status: StatusEnum["APPROVED"]
    })

    db.commit()

    payment = db.query(Payment).filter_by(id=payment_id).first()
    if payment:
        hashed_doc = payment_hash_table.get_payment_by_id(
            payment.id)
        if hashed_doc:
            payment_hash_table.delete_payment(payment.id)

    return payment


def reject_payment_by_id(db: Session, payment_id: int, current_user: UserOut):
    if (current_user.role.name != 'rt'):
        raise HTTPException(
            status_code=401, detail="Reject only can be done by admin"
        )

    payment = db.query(Payment).filter_by(id=payment_id)
    if not payment:
        raise HTTPException(
            status_code=404, detail="Data not found"
        )

    payment.update({
        Payment.status: StatusEnum["REJECTED"]
    })

    db.commit()

    payment = db.query(Payment).filter_by(id=payment_id).first()
    if payment:
        hashed_doc = payment_hash_table.get_payment_by_id(
            payment.id)
        if hashed_doc:
            payment_hash_table.delete_payment(payment.id)

    return payment


def update_payment_by_id(db: Session, payment_id: int, funding_id: int, funding_name: str, request: PaymentInUpdate, current_user: UserOut):
    payment = db.query(Payment).filter_by(id=payment_id)
    owner_id = current_user.id

    if request.file:
        payments = get_all_payment(db, funding_id, '', current_user)
        generated_filename = generate_unique_filename(
            f'{current_user.name}_{funding_name}', payments, owner_id, payment_id)

        file_path = f'{generated_filename}.pdf'
        with open(file_path, "wb") as file_object:
            file_object.write(request.file.file.read())

        payment.update({
            Payment.file_path: file_path
        })

    db.commit()
    payment = db.query(Payment).filter_by(id=payment_id).first()
    if payment:
        hashed_doc = payment_hash_table.get_payment_by_id(payment.id)
        if hashed_doc:
            payment_hash_table.delete_payment(payment.id)

    return payment


def generate_unique_filename(title, payments, owner_id, id=None):
    save_path = 'public/uploads/'
    registered_filename = set()
    for row in payments:
        if (id and id != row.id):
            registered_filename.add(row.file_path.replace('.pdf', ''))
        elif (not id):
            registered_filename.add(row.file_path.replace('.pdf', ''))
    filename = f'{owner_id}_{title}'
    generated_filename = f'{save_path}{filename}'
    if generated_filename in registered_filename:
        idx = 0
        generated_filename = f'{save_path}{filename}_{idx}'
        while generated_filename in registered_filename:
            idx += 1
            generated_filename = f'{save_path}{filename}_{idx}'
    return generated_filename


def delete_payment_by_id(db: Session, id: int, current_user: UserOut):
    payment_hash_table.delete_payment(id)
    payment = get_payment_by_id(db, id, current_user)
    if payment:
        db.delete(payment)
        db.commit()
        return payment
    else:
        raise HTTPException(
            status_code=200, detail=f"Payment with ID {id} not found")
